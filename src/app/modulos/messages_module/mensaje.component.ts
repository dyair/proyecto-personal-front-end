import { Component, OnInit } from '@angular/core';
import { Comunicacion } from 'src/app/modelos/modelos';

@Component({
  selector: 'app-mensaje',
  templateUrl: './mensaje.component.html',
  styleUrls: ['./mensaje.component.css']
})

export class MensajeComponent implements OnInit {

  private comunicacion: Comunicacion;

  constructor() {

  }

  ngOnInit() {

  }

  getChat(com: Comunicacion) {

    this.comunicacion = com;

  }

}

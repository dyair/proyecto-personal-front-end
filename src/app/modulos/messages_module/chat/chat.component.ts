import { Component, OnInit, AfterViewChecked, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Mascota_http_Service } from 'src/app/servicios/mascota_http.service';
import { Subscription } from 'rxjs';
import { PetService } from 'src/app/servicios/pet.service';
import { flatMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Comunicacion, Mascota, Mensaje } from 'src/app/modelos/modelos';
import { WebsocketService } from 'src/app/servicios/sockets/websocket.service';

declare const $;

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})

export class ChatComponent implements OnInit, AfterViewChecked, OnDestroy {

  private comunicacion_chat: Comunicacion;
  private contenedor: HTMLElement;
  private subscripcionSocketMensaje: Subscription;
  private subscripcionComunicacionMensaje: Subscription;
  private sms_contenido: FormControl;
  private mascota: Mascota;
  private mascota_ajena: Mascota;
  private mensajes: Mensaje[];
  private scrollAlInicio: boolean;  // bandera para evitar el scrolleo luego de apretar 'ver más'.
  private fecha_desde_sms: Date;

  constructor(private http_srv: Mascota_http_Service, private router: Router, private webSocket: WebsocketService, private pet_srv: PetService) {

  }

  ngOnInit() {

    this.mensajes = [];

    if (this.pet_srv.getComElegidaStorage()) {

      let com_storage_guardada: Comunicacion = this.pet_srv.getComElegidaStorage();
      this.pet_srv.setComMascota(com_storage_guardada);

    }

    // Traer los mensajes de la comunicación (los últimos 30 trae).
    this.subscripcionComunicacionMensaje = this.pet_srv.com_elegida
      .pipe(flatMap((com: Comunicacion) => {

        this.mascota = this.pet_srv.getMascotaStorage();

        if (com.mascota_destino.id_mascota === this.mascota.id_mascota) this.mascota_ajena = com.mascota_origen;
        else this.mascota_ajena = com.mascota_destino;

        this.comunicacion_chat = com;
        this.sms_contenido = new FormControl('');

        return this.http_srv.get(`/mascota/${this.mascota.id_mascota}/comunicacion/${com.id_comunicacion}/mensajes/${new Date()}`);

      }))
      .subscribe((mensajes: Mensaje[]) => {

        this.fecha_desde_sms = mensajes[0].fecha;

        for (let i = 0; i < mensajes.length; i++) {

          this.mensajes.push(mensajes[i]);

        }

      }, (error: Error) => {
        console.error(error.message);
        this.router.navigateByUrl('/social/contacts');
      });

    // Agregar un nuevo mensaje al chat proveniente del servidor usando sockets.
    this.subscripcionSocketMensaje = this.pet_srv.mensaje_mascota
      .subscribe((sms: Mensaje) => {

        try {

          if (sms) this.mensajes.push(sms);

        } catch (error) { }

      }, (e: Error) => {
        console.log(e.message);
      });

    // aumentar textarea luego de un enter o renglón escrito automáticamente.
    $("textarea").keyup(function (e) {
      while ($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
        $(this).height($(this).height() + 1);
      }
    });

    this.scrollAlInicio = false;

  }

  ngAfterViewChecked() {

    if (!this.scrollAlInicio) {
      try {
        this.contenedor = document.getElementById('contenedorChat');
        this.contenedor.scrollTop = this.contenedor.scrollHeight;
      } catch (e) { }
    }

  }

  ngOnDestroy() {

    this.subscripcionSocketMensaje.unsubscribe();
    this.subscripcionComunicacionMensaje.unsubscribe();
    this.pet_srv.setComElegidaStorage(undefined);
    this.pet_srv.removeComElegidaStorage();

    let sms_no_leidos: number[] = [];

    if (this.mensajes.length > 0) {

      this.mensajes.forEach(mensaje => {
        if (mensaje.mascota_destino_id === this.mascota.id_mascota && !mensaje.visto_destino) sms_no_leidos.push(mensaje.id_mensaje);
      });

    }

    if (sms_no_leidos.length === 0) return;

    const objeto = {
      id_mensajes: sms_no_leidos
    };

    this.http_srv.put(`/mascota/${this.mascota.id_mascota}/set_sms_visto`, objeto)
      .subscribe(() => {

        console.log('Mensajes ya vistos :)');

      }, (e: Error) => {
        console.error(e.message);
      });

  }

  mandarSms() {

    if (this.sms_contenido.value) {

      let mensaje: Mensaje = {
        contenido: this.sms_contenido.value,
        mascota_destino_id: this.mascota_ajena.id_mascota,
        mascota_origen_id: this.mascota.id_mascota,
        cabecera_sms_id: this.comunicacion_chat.id_comunicacion,
        fecha: new Date()
      };

      this.webSocket
        .enviarDatos(this.webSocket.getEnviar_nuevo_mensaje(), mensaje, (data: Mensaje) => {

          this.sms_contenido.reset();
          this.mensajes.push(data);

        });

    }
  }

  irContactos() {
    this.router.navigateByUrl('/social/contacts');
  }

  irMascotaAjena(pet: Mascota) {

    this.pet_srv.setMascotaAjena(pet);
    this.router.navigateByUrl('/pet');

  }

  traerMensajes() {

    this.scrollAlInicio = true;

    this.http_srv.get(`/mascota/${this.mascota.id_mascota}/comunicacion/${this.comunicacion_chat.id_comunicacion}/mensajes/${this.fecha_desde_sms}`)
      .subscribe((mensajes: Mensaje[]) => {

        if (mensajes.length > 0) {

          this.mensajes = mensajes.concat(this.mensajes);
          this.fecha_desde_sms = mensajes[0].fecha;
          this.contenedor.scrollTo(0, 85);  // scrollea en no responsivo.

        } else {

          $('#verMasMensajes').prop('disabled', true);
          document.getElementById('noMasMensajes').innerHTML = `<span class="badge badge-pill badge-warning mb-4">Inicio de conversación</span>`;

        }

      }, (error: Error) => {

        console.log(error);

        document.getElementById('sms_error').innerHTML = `
        <div class="alert alert-danger alert-dismissible col-8 fade show" role="alert">
            Error obteniendo mensajes, intente luego.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        `;

      });
  }

}

import { Routes, RouterModule } from '@angular/router';

import { ContactosComponent } from './contactos/contactos.component';
import { ChatComponent } from './chat/chat.component';

import { ComunicacionGuardService } from 'src/app/servicios/guards/pet-guard.service';

const APP_ROUTES: Routes = [

    {
        path: 'contacts',
        component: ContactosComponent
    },
    {
        path: 'messages',
        canActivate: [ComunicacionGuardService],
        component: ChatComponent
    }

];

export const APP_ROUTING_MENSAJES = RouterModule.forChild(APP_ROUTES);
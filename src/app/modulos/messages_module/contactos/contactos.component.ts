import { Component, OnInit } from '@angular/core';
import { Comunicacion, Mascota } from 'src/app/modelos/modelos';
import { Router } from '@angular/router';
import { Mascota_http_Service } from 'src/app/servicios/mascota_http.service';
import { PetService } from 'src/app/servicios/pet.service';

@Component({
  selector: 'app-contactos',
  templateUrl: './contactos.component.html',
  styleUrls: ['./contactos.component.css']
})

export class ContactosComponent implements OnInit {

  private comunicaciones: Comunicacion[];
  private mascota: Mascota;
  private spinner_loading: boolean;

  constructor(private http_srv: Mascota_http_Service, private pet_srv: PetService, private router: Router) { }

  ngOnInit() {

    this.spinner_loading = true;
    this.mascota = this.pet_srv.getMascotaStorage();

    this.http_srv.get(`/mascota/${this.mascota.id_mascota}/comunicacion`)
      .subscribe((comunicaciones: Comunicacion[]) => {

        this.comunicaciones = [];

        for (let i = 0; i < comunicaciones.length; i++) {

          this.comunicaciones.push(comunicaciones[i]);
          this.comunicaciones[i].ultimo_mensaje = comunicaciones[i].mensajes[0];

        }

        this.spinner_loading = false;

      }, (e) => {
        this.spinner_loading = false;
        console.log(e.error);
      });

  }

  elegirComunicacion(com_: Comunicacion) {

    this.pet_srv.setComMascota(com_);

    this.router.navigate(['social', 'messages']);

  }

}

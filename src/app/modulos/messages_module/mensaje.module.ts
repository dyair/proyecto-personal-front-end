import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { APP_ROUTING_MENSAJES } from './mensajes.routes';

// Componentes
import { ChatComponent } from './chat/chat.component';
import { ContactosComponent } from './contactos/contactos.component';

// Servicios
import { ComunicacionGuardService } from '../../servicios/guards/pet-guard.service';


@NgModule({
    declarations: [
        ChatComponent,
        ContactosComponent
    ],

    imports: [
        CommonModule,
        APP_ROUTING_MENSAJES,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
    ],

    providers:[
        ComunicacionGuardService
    ]
})

export class MensajeModule { }

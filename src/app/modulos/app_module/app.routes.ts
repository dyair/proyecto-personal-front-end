import { Routes, RouterModule } from '@angular/router';

import { LoginGuardService } from '../../servicios/guards/login-guard.service';

import { LoginComponent } from './usuario/login/login.component';
import { RegistrarComponent } from './usuario/registrar/registrar.component';

const APP_ROUTES: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegistrarComponent },
    {
        path: '',
        canLoad: [LoginGuardService],
        loadChildren: () => import('../../modulos/inicio_module/inicio.module')
            .then(m => m.InicioModule)
    },

    // otherwise redirect to login
    { path: '**', pathMatch: 'full', redirectTo: '/login' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);

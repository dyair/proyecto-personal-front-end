import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Usuario, Error_mensaje } from 'src/app/modelos/modelos';
import { Mascota_http_Service } from 'src/app/servicios/mascota_http.service';
import { parametrizacion_sms_error, mensaje_error } from 'src/environments/environment.prod';
import { PetService } from '../../../../servicios/pet.service';
import { UserService } from '../../../../servicios/usuario.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})

export class RegistrarComponent implements OnInit {

  private formularioRegistro: FormGroup;
  private pwValidada: boolean;

  constructor(private formBuilder: FormBuilder, private usr_srv: UserService, private router: Router, private _regSrv: Mascota_http_Service) { }

  ngOnInit() {

    // Local
    // example@example.com 
    // example123

    this.pwValidada = false;

    this.formularioRegistro = this.formBuilder.group({
      usuario: (['', Validators.required]),
      correo: (['', [Validators.required, Validators.pattern('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$')]]),
      password: (['', [Validators.required, Validators.minLength(6)]]),
      confirmPassword: (['', Validators.required])
    },
      {
        validator: this.mustMatch('password', 'confirmPassword')
      });

    this.validarInputsPw();

  }

  mustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }

  repetirPw(pw1: string, pw2: string): boolean {
    //Para settear la bandera y darle clases a los input en caso de error o acierto al completar las pw.
    if (pw1 === '' || pw2 === '' || pw1 === ' ' || pw2 === ' ') {
      return false;
    } else if (pw1 === pw2) {
      return true;
    } else {
      return false;
    }
  }

  registrarme() {

    let usuario: Usuario = { nombre: '', password: '', email: '', google: false };
    usuario.nombre = this.formularioRegistro.value.usuario;
    usuario.password = this.formularioRegistro.value.password;
    usuario.email = this.formularioRegistro.value.correo;

    const usu = { usuario }

    let us: string = JSON.stringify(usu);

    this._regSrv.postJSON_no_auth('/inicio/registrar', us)
      .subscribe((rdo: Usuario) => {

        this.usr_srv.setUsuarioStorage(rdo);
        this.router.navigateByUrl('/browser');

      }, (error: Error) => {
        
        let sms_error: Error_mensaje = parametrizacion_sms_error(error);
        document.getElementById('sms_error').innerHTML = mensaje_error(sms_error.mensaje);;

      });

  }

  validarInputsPw() {
    //Jugar con los input contraseña y confirmar contraseña.
    this.formularioRegistro.get('confirmPassword').disable();
    this.formularioRegistro.get('password').valueChanges.subscribe(() => {

      if (this.formularioRegistro.get('password').value === '') {
        this.formularioRegistro.get('confirmPassword').reset();
        this.formularioRegistro.get('confirmPassword').disable();
      } else {
        this.formularioRegistro.get('confirmPassword').enable();
        this.pwValidada = this.repetirPw(this.formularioRegistro.get('confirmPassword').value, this.formularioRegistro.get('password').value);
      }
    });

    this.formularioRegistro.get('confirmPassword').valueChanges.subscribe(() => {
      this.pwValidada = this.repetirPw(this.formularioRegistro.get('confirmPassword').value, this.formularioRegistro.get('password').value);
    });
  }

}

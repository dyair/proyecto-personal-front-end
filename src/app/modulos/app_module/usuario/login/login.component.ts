import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Mascota_http_Service } from 'src/app/servicios/mascota_http.service';
import { environment, parametrizacion_sms_error, mensaje_error } from 'src/environments/environment.prod';
import { UserService } from 'src/app/servicios/usuario.service';
import { Usuario } from 'src/app/modelos/modelos';
import { Error_mensaje } from '../../../../modelos/modelos';


declare const gapi;
declare const $;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  public auth2;
  private formularioRegistro: FormGroup;
  private pwValidada: boolean;
  private formularioInicio: FormGroup;

  constructor(private formBuilder: FormBuilder, private us_srv: UserService, private router: Router, private _regSrv: Mascota_http_Service) { }

  ngOnInit() {

    this.us_srv.limpiarStorage();

    // Hacer funcionar el tooltip.
    $(document).ready(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });

    this.googleInit();

    this.formularioInicio = this.formBuilder.group({
      correo: (['example@example.com', Validators.required]),
      password: (['example123', [Validators.required, Validators.minLength(6)]])
    });

  }

  iniciarSesion() {

    console.log(environment.linkUrl);

    let usuario = {
      email: this.formularioInicio.value.correo,
      password: this.formularioInicio.value.password
    }

    this._regSrv.postJSON_no_auth('/inicio/login', usuario)
      .subscribe((usuario: Usuario) => {

        this.us_srv.setUsuarioStorage(usuario);
        this.router.navigate(['/browser']);

      }, (e: Error) => {

        let sms_error: Error_mensaje = parametrizacion_sms_error(e);

        document.getElementById('sms_error').innerHTML = mensaje_error(sms_error.mensaje);
      });

  }

  // ------------------------------------------
  // Google
  // ------------------------------------------

  googleInit() {

    gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        client_id: environment.CLIENT_ID,
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });

      this.attachSignIn(document.getElementById('btnGoogle'));
    });

  }

  attachSignIn(elemento: any) {

    this.auth2.attachClickHandler(elemento, {}, async (googleUser: any) => {

      let token = await googleUser.getAuthResponse().id_token;

      this._regSrv.postJSON('/inicio/login_google', { token })
        .subscribe((rdo: Usuario) => {

          // Error de angular que no responde con adentro de una función pura de js.
          this.router.navigateByUrl('/browser');

        });
    }, ((e: Error) => console.error(e)));
  }

  // ------------------------------------------
  // Facebook
  // -----------------------------------------

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { PetService } from '../../../servicios/pet.service';
import { Mascota } from 'src/app/modelos/modelos';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})

export class BusquedaComponent implements OnInit, OnDestroy {

  private observable_busqueda: Subscription;
  private mascotas: Mascota[];
  private page: number = 1;
  private pageSize: number = 6;

  constructor(private pet_srv: PetService, private router: Router) { }

  ngOnInit() {

    this.observable_busqueda = this.pet_srv.busqueda_mascotas
      .subscribe((mascotas: Mascota[]) => {

        this.mascotas = [];
        this.mascotas = mascotas;

        this.mascotas.forEach(pet => {
          if (!pet.profile_pic) pet.profile_pic = environment.profile_pic_default;
        });

      }, (e) => {
        console.log(e.error);
      });

  }

  ngOnDestroy() {
    this.observable_busqueda.unsubscribe();
  }

  cambiarEstado() {
    console.log('cambiando estado');
  }

  irMascota(mascota: Mascota) {

    this.pet_srv.setMascotaAjena(mascota);
    this.router.navigateByUrl('/pet');

  }

}

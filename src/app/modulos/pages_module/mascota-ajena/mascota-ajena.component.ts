import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Mascota_http_Service } from 'src/app/servicios/mascota_http.service';
import { Mascota, Mensaje, Comunicacion } from '../../../modelos/modelos';
import { PetService } from '../../../servicios/pet.service';
import { FormControl, Validators } from '@angular/forms';
import { flatMap } from 'rxjs/operators';

declare const $;

@Component({
  selector: 'app-mascota-ajena',
  templateUrl: './mascota-ajena.component.html',
  styleUrls: ['./mascota-ajena.component.css']
})

export class MascotaAjenaComponent implements OnInit, OnDestroy, AfterViewInit {

  private mascota: Mascota;
  private mascota_propia: Mascota;
  private sms_contenido: FormControl;
  private spinner_loading: boolean;
  private cant_post: number;

  constructor(private http_pet: Mascota_http_Service, private pet_srv: PetService) {

  }

  ngOnInit() {

    this.spinner_loading = true;
    this.sms_contenido = new FormControl('', [Validators.required]);

    this.mascota = this.pet_srv.getMascotaAjenaStorage();
    this.mascota_propia = this.pet_srv.getMascotaStorage();

    // traer la cantidad total de post de una mascota.
    this.http_pet.get(`/mascota/${this.mascota.id_mascota}/publicacion/cantidad_post`)
      .subscribe((cantidad_posteos: number) => {
        this.cant_post = cantidad_posteos;
      }, (e) => {
        console.error(e.error);
      });

  }

  ngAfterViewInit() {

    // Sin esto el toast se visualiza siempre, es raro porque el toast('show') solo lo activo cuando mando el sms.
    document.getElementById('toast_id').style.visibility = "hidden";

  }

  ngOnDestroy() {
    this.pet_srv.setMascotaAjena(undefined);
    this.pet_srv.removeMascotaAjenaElegidaStorage();
  }

  enviarMensaje() {

    if (!this.sms_contenido.value) return;

    let comunicacion: Comunicacion = {
      mascota_destino_id: this.mascota.id_mascota,
      mascota_origen_id: this.mascota_propia.id_mascota,
    }

    const com_ = { comunicacion };

    this.http_pet.postJSON(`/mascota/${this.mascota_propia.id_mascota}/comunicacion`, JSON.stringify(com_))
      .pipe(flatMap((com: Comunicacion) => {

        let mensaje: Mensaje = {
          contenido: this.sms_contenido.value,
          mascota_destino_id: this.mascota.id_mascota,
          mascota_origen_id: this.mascota_propia.id_mascota,
          cabecera_sms_id: com.id_comunicacion,
          fecha: new Date(),
          visto_destino: false,
          visto_origen: true
        }

        const sms = { mensaje };

        return this.http_pet.postJSON(`/mascota/${this.mascota_propia.id_mascota}/comunicacion/${com.id_comunicacion}/mensaje`, JSON.stringify(sms));

      }))
      .subscribe((sms: Mensaje) => {

        console.log(sms);

      }, (e: Error) => {
        console.error(e.message);
      });

  }

  refreshNumberPost(numero: number) {
    this.cant_post++;
  }

}

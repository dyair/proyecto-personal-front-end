import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";

// Componentes
import { MascotahomeComponent } from './mascotahome/mascotahome.component';
import { MascotaAjenaComponent } from './mascota-ajena/mascota-ajena.component';
import { ListaPostComponent } from './posteos-mascotas/lista-post/lista-post.component';
import { HacerPostComponent } from './posteos-mascotas/hacer-post/hacer-post.component';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { MensajeComponent } from '../messages_module/mensaje.component';

// Rutas
import { APP_ROUTING_PAGES } from "./paginas.routes";

// Modulos
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { CompartidoModule } from '../shared_module/compartido.module';

// Servicios
import * as Servicio_guard from 'src/app/servicios/guards/pet-guard.service';

@NgModule({

    declarations: [
        MensajeComponent,
        MascotahomeComponent,
        MascotaAjenaComponent,
        ListaPostComponent,
        HacerPostComponent,
        BusquedaComponent
    ],

    imports: [
        ReactiveFormsModule,
        FormsModule,
        CommonModule,
        CompartidoModule,
        APP_ROUTING_PAGES,
        NgbModule,
        InfiniteScrollModule,
        NgbPaginationModule
    ],

    providers: [
        Servicio_guard.PetGuardService,
        Servicio_guard.PetAjenaGuardService
    ]
})

export class PageModule { }
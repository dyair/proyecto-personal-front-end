import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Mascota, Post } from '../../../modelos/modelos';
import { PetService } from '../../../servicios/pet.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Mascota_http_Service } from '../../../servicios/mascota_http.service';

declare const $;

@Component({
  selector: 'app-mascotahome',
  templateUrl: './mascotahome.component.html',
  styleUrls: ['./mascotahome.component.css']
})

export class MascotahomeComponent implements OnInit {

  private mascota: Mascota;
  private post: Post;
  private cant_post: number;
  private hoy: string;
  private actualizarDatos: FormGroup;
  private imagen_previsualizada: string | ArrayBuffer;
  private imagen_archivo: File;

  constructor(private cdRef: ChangeDetectorRef, private pet_srv: PetService, private http_srv: Mascota_http_Service, private formBuilder: FormBuilder) {

  }

  ngOnInit() {

    this.mascota = this.pet_srv.getMascotaStorage();

    this.hoy = new Date().toISOString().slice(0, 10);

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });

    this.actualizarDatos = this.formBuilder.group({
      nombre_mascota: ([this.mascota.nombre_mascota, [Validators.required, Validators.minLength(2)]]),
      fecha_nac: ([new Date(this.mascota.fecha_nac).toISOString().slice(0, 10), Validators.required]),
      biografia: ([this.mascota.biografia || '', [Validators.required]])
    });

    // indica la cantidad de posteos que tiene en total la mascota.
    this.http_srv.get(`/mascota/${this.mascota.id_mascota}/publicacion/cantidad_post`)
      .subscribe((cantidad: number) => {

        this.cant_post = cantidad;

      }, (e: Error) => {
        console.error(e.message);
      });
  }

  agregar_nuevo_post(post: Post) {

    this.post = post;

  }

  actualizarContadorPost(cantidad: number) {

    this.cant_post = this.cant_post + cantidad; //siempre cantidad es 1;
    this.cdRef.detectChanges();

  }

  actualizarPerfil() {

    let data: FormData = new FormData();

    data.append('nombre_mascota', this.actualizarDatos.get('nombre_mascota').value);
    data.append('biografia', this.actualizarDatos.get('biografia').value);
    data.append('fecha_nac', this.actualizarDatos.get('fecha_nac').value);
    data.append('tamanio_id', JSON.stringify(this.mascota.tamanio.id_tamanio));
    data.append('sexo_id', JSON.stringify(this.mascota.sexo.id_sexo));
    data.append('tipo_id', JSON.stringify(this.mascota.tipo.id_tipo_mascota));
    data.append('raza_id', JSON.stringify(this.mascota.raza.id_raza));
    data.append('temperamento_id', JSON.stringify(this.mascota.temperamento.id_temperamento));
    data.append('domicilio_id', JSON.stringify(this.mascota.domicilio.id_domicilio));
    data.append('usuario_id', JSON.stringify(this.mascota.usuario.id));

    if (this.imagen_archivo) data.append('imagen', this.imagen_archivo)
    else data.append('profile_pic', this.mascota.profile_pic);

    // put no soporta formData, por eso uso el post con el id.
    this.http_srv.postFormData(`/mascota/${this.mascota.id_mascota}`, data)
      .subscribe((rdo: Mascota) => {

        this.pet_srv.setMascotaPropia(rdo);
        this.mascota = rdo;

      }, (e: Error) => {
        console.error(e.message);
      });

  }

  setImagenPerfilMascota(imagen: File) {

    this.imagen_archivo = imagen;

    let path_foto: string = $('#customFile').val();
    let nombre_foto: string = path_foto.slice(12, path_foto.length);
    let input_customFile: any = document.getElementById('customFile');

    document.getElementById('label_id').textContent = nombre_foto;

    let reader = new FileReader();

    reader.readAsDataURL(imagen);
    reader.onload = (_event) => {

      this.imagen_previsualizada = reader.result;
      input_customFile.value = '';

    }
  }

  eliminarImagen() {

    this.imagen_previsualizada = undefined;
    this.imagen_archivo = undefined;
    document.getElementById('label_id').textContent = 'Elegir foto';

  }

}

import { Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { Mascota, Post } from '../../../../modelos/modelos';
import { Mascota_http_Service } from '../../../../servicios/mascota_http.service';
import { HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';

declare const $;

@Component({
  selector: 'app-lista-post',
  templateUrl: './lista-post.component.html',
  styleUrls: ['./lista-post.component.css']
})

export class ListaPostComponent implements OnInit, OnChanges, AfterViewInit {

  @Output() cant_post = new EventEmitter<number>();
  @Input() mascota: Mascota;
  @Input() post_nuevo: Post;

  private posteos: Post[];
  private contenedor: HTMLElement;
  private post_a_borrar: Post;
  private ultima_fecha_post: Date;
  private spinner_loading: boolean;

  constructor(private pet_srv: Mascota_http_Service, private router: Router) { }

  ngOnInit() {

    this.spinner_loading = true;

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });

    // El listado de post siempre se maneja por input porque pueden ser post propios o de otra mascota.

    this.ultima_fecha_post = new Date();

    this.posteos = [];

    this.getPosteos();

  }

  ngAfterViewInit() {
    // este if debería funcionar pero por algún motivo no lo toma luego de cargar el DOM. Parche: *ngIf en el html.
    // if (this.router.url === '/pet')  document.getElementById('eliminar_post_boton').style.visibility = 'hidden';
  }

  ngOnChanges(cambios: SimpleChanges) {

    if (cambios.mascota) this.mascota = cambios.mascota.currentValue;

    if (cambios.post_nuevo) {

      this.post_nuevo = cambios.post_nuevo.currentValue;
      
      if (this.post_nuevo) {
        this.posteos.unshift(this.post_nuevo);
        this.cant_post.emit(1);
      }

    }

  }

  seleccionarPostBorrar(p: Post) {
    this.post_a_borrar = p;
  }

  eliminarPost() {

    if (!this.post_a_borrar) return;

    this.pet_srv.delete(`/mascota/${this.mascota.id_mascota}/publicacion`, this.post_a_borrar.id_post)
      .subscribe((id_post: number) => {

        for (let i = 0; i < this.posteos.length; i++) {

          if (id_post === this.posteos[i].id_post) {
            this.posteos.splice(i, 1);
            this.post_a_borrar = undefined;
            break;
          }

        }

      }, (e: Error) => console.error(e.message));

  }

  getPosteos() {

    let parametros: HttpParams = new HttpParams()
      .set('fecha_desde', JSON.stringify(this.ultima_fecha_post));

    // Límite de 20 post en cada request.
    this.pet_srv.getSearch(`/mascota/${this.mascota.id_mascota}/publicacion`, parametros)
      .subscribe((datos: Post[]) => {
        
        this.spinner_loading = false;

        if (datos.length === 0) {

          try {
            document.getElementById('verMasPost').style.display = 'none';
            document.getElementById('noMasPost').innerHTML = `<span class="badge badge-pill badge-warning mb-4">No hay más publicaciones</span>`;
          } catch (error) { }

          return;

        }

        for (let i: number = 0; i < datos.length; i++) {

          this.posteos.push(datos[i]);

          if (i === (datos.length - 1)) this.ultima_fecha_post = new Date(datos[i].fecha_publicacion);

        }

        // desde el ngOnChanges se encarga de actualizar el contador de posteos del perfil de la mascota 
        // después del submit del post con el emit().

      }, (e: Error) => {
        this.spinner_loading = false;
        console.log(e.message);
      });
  }

}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Mascota, Post } from 'src/app/modelos/modelos';
import { Mascota_http_Service } from 'src/app/servicios/mascota_http.service';

declare const $;

@Component({
  selector: 'app-hacer-post',
  templateUrl: './hacer-post.component.html',
  styleUrls: ['./hacer-post.component.css']
})

export class HacerPostComponent implements OnInit {

  @Output() post_nuevo = new EventEmitter<Post>();

  private post: FormGroup;
  private imagenes: File[];
  private imgHTML: any[];
  private mascota: Mascota;

  constructor(private srv: Mascota_http_Service, private fb: FormBuilder) { }

  ngOnInit() {

    this.imagenes = [];
    this.imgHTML = [];
    this.mascota = JSON.parse(sessionStorage.getItem('mascota'));

    let limite_max_caract: number = 140;
    let progreso: number = 0;
    let rdo: number = 0;

    $(document).ready(function () {

      // No permitir post vacíos
      $("#post_button").attr("disabled", true);

      // Hacer funcionar el tooltip.
      $('[data-toggle="tooltip"]').tooltip();

      // Implementar progress limit.
      $('#text_area').on('input', function () {

        progreso = $('#text_area').val().length;

        if (progreso >= limite_max_caract || progreso === 0) {
          $("#post_button").attr("disabled", true);
        } else {
          $("#post_button").attr("disabled", false);
          rdo = Number((progreso / limite_max_caract).toFixed(2));

          if (rdo < 0.95) {
            $(".progress-bar").css("width", (rdo * 100) + "%");
            $(".progress-bar").removeClass('bg-danger').addClass('bg-info');
          } else {
            $(".progress-bar").css("width", (rdo * 100) + "%");
            $(".progress-bar").removeClass('bg-info').addClass('bg-danger');
          }
        }
      });

    });

    this.post = this.fb.group({
      descripcion: ['', Validators.required],
      fecha_publicacion: [new Date(), Validators.required],
      mascota_id: [this.mascota.id_mascota, Validators.required]
    });

  }

  hacerPost() {

    //Pasar esto al componente de post mejor. Dejar el home un poco más limpio.
    const post: FormData = new FormData();

    let publicacion: Post = {
      descripcion: this.post.get('descripcion').value,
      mascota_id: this.post.get('mascota_id').value
    };

    this.imagenes.forEach(imagen => {

      post.append('imagenes', imagen, imagen.name);

    });

    post.append('descripcion', publicacion.descripcion);

    this.srv.postFormData('/mascota/' + publicacion.mascota_id + '/publicacion', post)
      .subscribe((data: Post) => {

        this.post_nuevo.emit(data);
        
        this.post.get('descripcion').reset();
        this.imgHTML = [];
        this.imagenes = [];
        $(".progress-bar").css("width", 0 + "%");
        $("#post_button").attr("disabled", true);

      });

  }

  getImage(evento: any) {

    let archivo_repetido: boolean = false;
    let imagen: File = evento.target.files[0];
    let html_input: any = document.getElementById("input_file");
    let nombre_archivo: string = this.getNameFile(html_input.value);

    if (imagen && this.imagenes.length < 3) {

      for (let i = 0; i < this.imagenes.length; i++) {
        if (nombre_archivo === this.imagenes[i].name) archivo_repetido = true
      }

      if (!archivo_repetido) {

        let reader = new FileReader();
        reader.readAsDataURL(imagen);
        reader.onload = (_event) => {

          this.imgHTML.push(reader.result);
          this.imagenes.push(imagen);

          html_input.value = '';

        }
      }
    }

  }

  eliminarImagen(posicion: number) {

    this.imagenes.splice(posicion, 1);
    this.imgHTML.splice(posicion, 1);

  }

  getNameFile(path_name: string): string {

    let posicion: number = path_name.search('h') + 2;
    let nombre: string = path_name.substr(posicion, path_name.length);

    return nombre;
  }

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { WebsocketService } from '../../servicios/sockets/websocket.service';
import { Subscription } from 'rxjs';
import { PetService } from '../../servicios/pet.service';
import { Mensaje, Comunicacion, Mascota } from '../../modelos/modelos';
import { Router } from '@angular/router';
import { flatMap } from 'rxjs/operators';
import { Mascota_http_Service } from '../../servicios/mascota_http.service';

declare const $;

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styles: []
})

export class PagesComponent implements OnInit, OnDestroy {

  private subscripcionMensajeSocket: Subscription;
  private mensaje: Mensaje;
  private com: Comunicacion;
  private mascota_ajenaSMS: Mascota;

  constructor(private wsSrv: WebsocketService, private pet_srv: PetService, private router: Router,
    private http_srv: Mascota_http_Service) { }

  ngOnInit() {

    // Solamente los listener son observables, los emit simplemente emiten y con el callback terminan.

    // SOCKETS
    const socket_pet = {
      id_mascota: this.pet_srv.getMascotaStorage().id_mascota,
      nombre_mascota: this.pet_srv.getMascotaStorage().nombre_mascota
    };

    this.wsSrv.enviarDatos(this.wsSrv.getNueva_mascota_socket(), socket_pet, (rdo: string) => console.log(rdo));

    /* LISTENERS */

    // El sms puede venir de la comunicacion actual en el observable u de otra nueva, por eso hay que revisar eso.
    // Probablemente el sms venga de otra comunicacion que no encesariamente sea la que está actualmente en el observable

    this.subscripcionMensajeSocket = this.wsSrv.escucharEventos(this.wsSrv.getLeer_nuevo_mensaje())
      .pipe(flatMap((mensaje: Mensaje) => {

        this.mensaje = mensaje;

        return this.http_srv.get(`/configuracion/comunicacion/${mensaje.cabecera_sms_id}`);

      }))
      .subscribe((com: Comunicacion) => {

        this.com = com;

        if (this.mensaje.mascota_destino_id === this.com.mascota_destino_id) {
          this.mascota_ajenaSMS = this.com.mascota_destino;
        } else {
          this.mascota_ajenaSMS = this.com.mascota_origen;
        }

        this.formaNotificarMensaje(this.mensaje, com);

      }, (e: Error) => {
        console.error(e.message);
      });
  }

  formaNotificarMensaje(sms: Mensaje, com: Comunicacion) {

    // Ver si lo pusheo como un toast o como un sms más del chat, depende de la ruta donde estoy.

    if (this.router.url === '/social/messages' && com.id_comunicacion === sms.cabecera_sms_id) {

      this.pet_srv.setSmsMascota(this.mensaje);
    } else {

      // activar una notificación en el navbar y mostrar el toast porque no esta sobre el chat del que viene el mensaje.
      document.getElementById('toast_sms').innerHTML = `
      <div class="toast" data-delay="3500">
        <div class="toast-header">
          <img src="${this.mascota_ajenaSMS.profile_pic}" class="rounded mr-2">
          <strong class="mr-auto">
              ${this.mascota_ajenaSMS.nombre_mascota}
          </strong>
          <small> ${new Date(this.mensaje.fecha).getHours() + ':' + this.getMinutos(this.mensaje.fecha)} </small>
        </div>
        <div class="toast-body">
          ${this.mensaje.contenido}
        </div>
      </div>
      <style>
          #toast_sms .toast {
              z-index: 99999;
              position: absolute;
              right: 0;
              top: 12;
              background-color: rgb(231, 230, 230);
              margin-right: 1.2%;
              width: 22vw;
          }

          #toast_sms .toast:hover {
            cursor: pointer;
          }

          #toast_sms .toast .toast-header img{
              width: 2.7vw;
              height: 2.7vw;
              border-radius: 50% !important;
              margin-right: 2%;
          }
      </style>`

      $('.toast').toast('show');
      this.pet_srv.setSmsMascota(this.mensaje);
    }

  }

  irComunicacion() {

    this.pet_srv.setComMascota(this.com);
    this.router.navigateByUrl('/social/messages');

  }

  //get minutos de la hs con formato 2 digitos.
  getMinutos(fecha: Date): string {

    let minutos: string;

    minutos = (new Date(fecha).getMinutes() < 10 ? '0' : '') + JSON.stringify(new Date(fecha).getMinutes());

    return minutos;

  }

  ngOnDestroy() {

    if (this.subscripcionMensajeSocket) this.subscripcionMensajeSocket.unsubscribe();   // Listener de sms socket
    this.pet_srv.setSmsMascota(undefined);          // Observable de mensajes de las mascota

  }

}

import { Routes, RouterModule } from '@angular/router';

import { MascotahomeComponent } from './mascotahome/mascotahome.component';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { MascotaAjenaComponent } from './mascota-ajena/mascota-ajena.component';
import { MensajeComponent } from '../messages_module/mensaje.component';

import * as Servicio_guard from '../../servicios/guards/pet-guard.service';

const APP_ROUTES: Routes = [

    // mascota de usuario
    {
        path: 'home',
        canActivate: [Servicio_guard.PetGuardService],
        component: MascotahomeComponent
    },
    { path: 'search', component: BusquedaComponent },

    // mascota ajena
    {
        path: 'pet',
        canActivate: [Servicio_guard.PetAjenaGuardService],
        component: MascotaAjenaComponent
    },
    {
        path: 'social',
        component: MensajeComponent,
        loadChildren: () => import('../messages_module/mensaje.module')
            .then(m => m.MensajeModule)
    },
    {
        // me parece que está de más, se carga al inicio por el navbar
        path: 'map',
        loadChildren: () => import('../shared_module/compartido.module')
            .then(m => m.CompartidoModule)
    },

    { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

export const APP_ROUTING_PAGES = RouterModule.forChild(APP_ROUTES);

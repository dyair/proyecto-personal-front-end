import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Mascota, Usuario } from 'src/app/modelos/modelos';
import { Mascota_http_Service } from 'src/app/servicios/mascota_http.service';
import { PetService } from '../../../servicios/pet.service';
import { UserService } from '../../../servicios/usuario.service';

declare const $;

@Component({
  selector: 'app-pagina',
  templateUrl: './browser.component.html',
  styleUrls: ['./browser.component.css']
})

export class BrowserComponent implements OnInit {

  private mascotas: Mascota[];

  constructor(private router: Router, private user_srv: UserService, private pet_srv: Mascota_http_Service, private pet_service: PetService) { }

  ngOnInit() {

    $('[data-toggle="tooltip"]').tooltip({
      trigger: 'hover'
    });

    this.pet_service.removeMascotaStorage();

    let user: Usuario = this.user_srv.getUsuarioStorage();

    this.pet_srv.get(`/usuario/${user.id}/mascota`)
      .subscribe((mascotas: Mascota[]) => {

        this.mascotas = [];

        mascotas.forEach(mascota => {
          this.mascotas.push(mascota);
        });

      }, (e: Error) => {
        console.error(e.message);
      });

  }

  irAgregarMascota() {

    $('.tooltip').tooltip('hide');  // forzar la destrucción del tooltip sino queda abierto.
    this.router.navigate(['/edit']);

  }

  irHome(m: Mascota) {

    this.pet_service.setMascotaPropia(m);
    this.router.navigateByUrl('/home');
    
  }

}

import { Routes, RouterModule } from '@angular/router';

import { BrowserComponent } from './browser/browser.component';
import { PagesComponent } from '../../modulos/pages_module/pages.component';

const APP_ROUTES: Routes = [

    {
        path: 'browser',
        component: BrowserComponent
    },
    {
        path: '',
        component: PagesComponent,
        loadChildren: () => import('../../modulos/pages_module/pages.module')
            .then(m => m.PageModule)
    },
    { path: '**', pathMatch: 'full', redirectTo: '/login' }

];

export const APP_ROUTING_INICIO = RouterModule.forChild(APP_ROUTES);
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { CompartidoModule } from "../shared_module/compartido.module";

// Rutas
import { APP_ROUTING_INICIO } from "./inicio.routes";

// Services
import { WebsocketService } from '../../servicios/sockets/websocket.service';
import { UserService } from "src/app/servicios/usuario.service";
import { LoginGuardService } from '../../servicios/guards/login-guard.service';
import { PetService } from "src/app/servicios/pet.service";

// Sockets. Lo tengo acá y no en PageModule porque lo implementa el PagesComponent.
import { environment } from '../../../environments/environment.prod';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const config: SocketIoConfig = { url: environment.linkUrl, options: { query: { token: new UserService().getUsuarioStorage().token  } } };

// Componentes
import { BrowserComponent } from './browser/browser.component';
import { PagesComponent } from "../pages_module/pages.component";

@NgModule({
    declarations: [
        BrowserComponent,
        PagesComponent
    ],

    imports: [
        ReactiveFormsModule,
        FormsModule,
        APP_ROUTING_INICIO,
        CommonModule,
        CompartidoModule,
        SocketIoModule.forRoot(config)
    ],

    providers: [
        WebsocketService,
        PetService,
        UserService,
        LoginGuardService
    ]
})

export class InicioModule { }
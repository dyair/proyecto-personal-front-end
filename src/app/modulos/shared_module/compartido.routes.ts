import { Routes, RouterModule } from '@angular/router';

import { MapComponent } from './map/map.component';
import { AddMascotaComponent } from './add-mascota/add-mascota.component';

const APP_ROUTES: Routes = [

    { path: 'view', component: MapComponent },
    { path: 'edit', component: AddMascotaComponent }


];

export const APP_ROUTING_COMPARTIDO = RouterModule.forChild(APP_ROUTES);

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Domicilio, Sexo, Raza, Pais, Temperamento, Mascota, Tamanio, Provincia } from 'src/app/modelos/modelos';
import { Mascota_http_Service } from 'src/app/servicios/mascota_http.service';
import { forkJoin, Subscription, Observable } from 'rxjs';
import { Usuario, Localidad } from '../../../modelos/modelos';
import { flatMap, switchMap } from 'rxjs/operators';
import { UserService } from 'src/app/servicios/usuario.service';
import { PetService } from '../../../servicios/pet.service';

declare const $;

@Component({
  selector: 'app-add-mascota',
  templateUrl: './add-mascota.component.html',
  styleUrls: ['./add-mascota.component.css']
})

export class AddMascotaComponent implements OnInit, OnDestroy {

  private registroMascotaForm: FormGroup;
  private registroDomMascotaForm: FormGroup;
  private sexos: Sexo[];
  private temperamentos: Temperamento[];
  private razas: Raza[];
  private paises: Pais[];
  private tamanios: Tamanio[];
  private provincias: Provincia[];
  private localidades: Localidad[];
  private tipos_mascotas: Provincia[];
  private imagen_previsualizada: string | ArrayBuffer;
  private imagen_archivo: File;
  private subscripcionMascota: Subscription;
  private spinner_loading: boolean;

  constructor(private formBuilder: FormBuilder, private router: Router, private srv: Mascota_http_Service,
    private us_srv: UserService, private pet_srv: PetService) {

  }

  ngOnInit() {

    this.spinner_loading = true;

    this.subscripcionMascota = new Subscription();
    let subscripcionMascotaInit: Subscription = new Subscription();

    subscripcionMascotaInit = this.pet_srv.mascota_propia
      .pipe(switchMap((pet: Mascota) => {

        let rdo: Mascota = (pet) ? pet : this.pet_srv.getMascotaStorage();

        if (rdo) {

          this.registroMascotaForm = this.formBuilder.group({
            nombre_mascota: ([rdo.nombre_mascota, [Validators.required, Validators.minLength(2)]]),
            fecha_nac: ([new Date(rdo.fecha_nac).toISOString().slice(0, 10), Validators.required]),
            tamanio_id: ([rdo.tamanio.id_tamanio, Validators.required]),
            sexo_id: ([rdo.sexo.id_sexo, Validators.required]),
            tipo_id: ([rdo.tipo.id_tipo_mascota, Validators.required]),
            raza_id: ([rdo.raza.id_raza, Validators.required]),
            tipo_mascota_id: ([rdo.tipo.id_tipo_mascota, Validators.required]),
            temperamento_id: ([rdo.temperamento.id_temperamento, Validators.required])
          });

          this.registroDomMascotaForm = this.formBuilder.group({
            calle: ([rdo.domicilio.calle, [Validators.required, Validators.minLength(2)]]),
            numero: ([rdo.domicilio.numero, [Validators.required, Validators.pattern('[0-9]*'), Validators.min(0)]]),
            piso: ([rdo.domicilio.piso, [Validators.pattern('[0-9]*'), Validators.min(0)]]),
            depto: ([rdo.domicilio.dpto]),
            localidad_id: ([rdo.domicilio.localidad_id, Validators.required]),
            pais_id: ([rdo.pais.id_pais, Validators.required]),
            provincia_id: ([rdo.provincia.id_provincia, Validators.required]),
          });

          return forkJoin([
            this.srv.get('/configuracion/sexo'),
            this.srv.get('/configuracion/tamanio'),
            this.srv.get('/configuracion/raza'),
            this.srv.get('/configuracion/pais'),
            this.srv.get('/configuracion/temperamento'),
            this.srv.get('/configuracion/tipo_mascota'),
            this.srv.get(`/configuracion/provincia/${rdo.pais.id_pais}`),
            this.srv.get(`/configuracion/localidad/${rdo.provincia.id_provincia}`)
          ]);

        } else {

          this.registroMascotaForm = this.formBuilder.group({
            nombre_mascota: (['', [Validators.required, Validators.minLength(2)]]),
            fecha_nac: (['', Validators.required]),
            tamanio_id: ([null, Validators.required]),
            sexo_id: ([null, Validators.required]),
            raza_id: ([null, Validators.required]),
            tipo_mascota_id: ([1, Validators.required]),
            temperamento_id: ([null, Validators.required])
          });

          this.registroDomMascotaForm = this.formBuilder.group({
            calle: (['', [Validators.required, Validators.minLength(2)]]),
            numero: ([null, [Validators.required, Validators.pattern('[0-9]*'), Validators.min(0)]]),
            piso: ([null, [Validators.pattern('[0-9]*'), Validators.min(0)]]),
            depto: ([null]),
            localidad_id: ([null, Validators.required]),
            provincia_id: ([null, Validators.required]),
            pais_id: ([5, Validators.required]),  // harcode. Review it.
          });

          return forkJoin([
            this.srv.get('/configuracion/sexo'),
            this.srv.get('/configuracion/tamanio'),
            this.srv.get('/configuracion/raza'),
            this.srv.get('/configuracion/pais'),
            this.srv.get('/configuracion/temperamento'),
            this.srv.get('/configuracion/tipo_mascota')
          ]);

        }

      }))
      .subscribe(([sexos, tamanios, razas, paises, temperamentos, tipos, provincias, localidades]) => {

        this.sexos = [];
        this.temperamentos = [];
        this.razas = [];
        this.paises = [];
        this.tamanios = [];
        this.tipos_mascotas = [];

        this.sexos = sexos;
        this.razas = razas;
        this.paises = paises;
        this.temperamentos = temperamentos;
        this.tamanios = tamanios;
        this.tipos_mascotas = tipos;

        if (localidades && provincias) {
          this.provincias = [];
          this.localidades = [];
          this.localidades = localidades;
          this.provincias = provincias;
        }

        this.spinner_loading = false;

        this.subscripcionMascota.add(subscripcionMascotaInit);

      }, (e: Error) => {
        this.spinner_loading = false;
        console.log(e.message + '. Error en editar los datos de la mascota en el forkjoin u observable de la mascota.');
      });

  }

  ngOnDestroy() {

    if (this.subscripcionMascota) this.subscripcionMascota.unsubscribe();

  }

  setImagenPerfilMascota(imagen: File) {

    this.imagen_archivo = imagen;

    let path_foto: string = $('#customFile').val();
    let nombre_foto: string = path_foto.slice(12, path_foto.length);
    let input_customFile: any = document.getElementById('customFile');

    document.getElementById('label_id').textContent = nombre_foto;

    let reader = new FileReader();

    reader.readAsDataURL(imagen);
    reader.onload = (_event) => {

      this.imagen_previsualizada = reader.result;
      input_customFile.value = '';

    }

  }

  eliminarImagen() {

    this.imagen_previsualizada = undefined;
    this.imagen_archivo = undefined;
    document.getElementById('label_id').textContent = 'Elegir foto';

  }

  submit() {

    let dom = { domicilio: this.registroDomMascotaForm.value };

    this.srv.postJSON('/configuracion/domicilio', dom)
      .pipe(flatMap((domicilio: Domicilio) => {

        let mascota_propia: Mascota = this.pet_srv.getMascotaStorage();
        let usuario: Usuario = this.us_srv.getUsuarioStorage();
        let mas_form: FormData = new FormData();
        let observable_retorno: Observable<Mascota>;

        mas_form.append('imagen', this.imagen_archivo);
        mas_form.append('nombre_mascota', this.registroMascotaForm.get('nombre_mascota').value);
        mas_form.append('fecha_nac', this.registroMascotaForm.get('fecha_nac').value);
        mas_form.append('tamanio_id', this.registroMascotaForm.get('tamanio_id').value);
        mas_form.append('sexo_id', this.registroMascotaForm.get('sexo_id').value);
        mas_form.append('raza_id', this.registroMascotaForm.get('raza_id').value);
        mas_form.append('temperamento_id', this.registroMascotaForm.get('temperamento_id').value);
        mas_form.append('tipo_mascota_id', this.registroMascotaForm.get('tipo_mascota_id').value);
        mas_form.append('domicilio_id', JSON.stringify(domicilio.id_domicilio));

        // Si hay una mascota en el storage es editar, sino crear una mascota nueva.
        if (!mascota_propia) observable_retorno = this.srv.postFormData('/usuario/' + usuario.id + '/mascota', mas_form);
        else observable_retorno = this.srv.postFormData('/mascota/' + mascota_propia.id_mascota, mas_form);

        return observable_retorno;

      }))
      .subscribe((mascota: Mascota) => {

        this.pet_srv.setMascotaPropia(mascota);
        this.registroDomMascotaForm.reset();
        this.registroMascotaForm.reset();
        this.router.navigate(['/home']);

      }, (e: Error) => {
        console.error(e.message);
      });
  }

  traerProvinciasByPais(id_pais: number) {

    if (id_pais === this.registroDomMascotaForm.value.id_pais) return;

    this.srv.get(`/configuracion/provincia/${id_pais}`)
      .subscribe((provincias: Provincia[]) => {

        this.provincias = [];
        this.localidades = [];
        this.provincias = provincias;

        this.registroDomMascotaForm.value.localidad_id = undefined;
        this.registroDomMascotaForm.value.provincia_id = undefined;

      }, (e: Error) => {
        console.error(e.message, '. Error trayendo provincias de un pais seleccionado.');
      });

  }

  traerLocalidadesByProvincias(id_prov: number) {

    if (id_prov === this.registroDomMascotaForm.value.localidad_id) return;

    this.srv.get(`/configuracion/localidad/${id_prov}`)
      .subscribe((localidades: Localidad[]) => {

        this.registroDomMascotaForm.value.localidad_id = undefined;
        this.localidades = [];
        this.localidades = localidades;

      }, (e: Error) => {
        console.error(e.message, '. Error trayendo provincias de un pais seleccionado.');
      });

  }

  volverAtras() {

    // dependiendo si está logueado o no con una mascota puede volver al home o sino al browser.
    let subscriptionMascotaVolverRouter: Subscription = new Subscription();

    subscriptionMascotaVolverRouter = this.pet_srv.mascota_propia
      .subscribe((rdo: Mascota) => {

        try {
          rdo = (rdo) ? rdo : this.pet_srv.getMascotaStorage();
        } catch (error) { }

        if (rdo) this.router.navigateByUrl('/home');
        else this.router.navigateByUrl('/browser');

        this.subscripcionMascota.add(subscriptionMascotaVolverRouter);

      }, (e: Error) => console.log(e.message));

  }

}

import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import { LeafletModule } from "@asymmetrik/ngx-leaflet";

import { APP_ROUTING_COMPARTIDO } from "./compartido.routes";

import { NavbarComponent } from "./navbar/navbar.component";
import { MapComponent } from "./map/map.component";
import { AddMascotaComponent } from "./add-mascota/add-mascota.component";

@NgModule({

    declarations: [
        NavbarComponent,
        MapComponent,
        AddMascotaComponent
    ],
    imports: [
        ReactiveFormsModule,
        CommonModule,
        LeafletModule.forRoot(),
        RouterModule,
        APP_ROUTING_COMPARTIDO
    ],
    exports: [
        NavbarComponent,
        MapComponent,
        AddMascotaComponent
    ]    
})

export class CompartidoModule { }
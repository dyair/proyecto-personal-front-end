import { Component, OnInit, OnDestroy } from '@angular/core';
import { Mascota } from '../../../modelos/modelos';
import * as L from "leaflet";
import { Router } from '@angular/router';
import { calcular_edad } from '../../../../environments/environment';
import { PetService } from 'src/app/servicios/pet.service';
import { Subscription } from 'rxjs';
import { flatMap, take, map } from 'rxjs/operators';
import { Mascota_http_Service } from 'src/app/servicios/mascota_http.service';
import { HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment.prod';

declare const $: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements OnInit, OnDestroy {

  private subcriptionPet: Subscription;
  private mascotas: Mascota[];
  private map: L.Map;
  private mascota_seleccionada: Mascota;
  private mascota: Mascota;

  constructor(private router: Router, private pet_srv: PetService, private http_srv: Mascota_http_Service) { }

  ngOnInit() {

    this.mascota = this.pet_srv.getMascotaStorage();

    this.subcriptionPet = this.pet_srv.busqueda_mascotas
      .pipe(take(1), flatMap((rdo: Mascota[]) => {

        if (typeof rdo === 'undefined') {

          console.log('Proviene del navbar sin ninguna búsqueda previa.');
          return this.http_srv.getSearch(`/mascota/${this.mascota.id_mascota}/search`, new HttpParams());

        } else if (rdo.length === 0) {

          console.log('No hubo resultados de la búsqueda :(.');
          return this.http_srv.getSearch(`/mascota/${this.mascota.id_mascota}/search`, new HttpParams().set('sexo', JSON.stringify(this.mascota.sexo.id_sexo)).set('provincia', JSON.stringify(this.mascota.provincia.id_provincia)))

        } else {
          console.log('Se buscaron mascotas y hubieron resultados :).');
          return this.pet_srv.busqueda_mascotas;
        }

      }))
      .pipe(map((mascotas: Mascota[]) => mascotas.filter(m => m.usuario.id !== this.mascota.usuario.id)))
      .subscribe((mascotas: Mascota[]) => {

        console.log('adentro del subscribe con las mascotas para iniciar el mapa');

        console.log('RDOS: ', mascotas);

        this.mascotas = [];
        this.mascotas = mascotas;

        this.iniciarMapa();

      }, (e) => {
        console.log(e.error);
      });

  }

  ngOnDestroy() {
    this.subcriptionPet.unsubscribe();
  }

  iniciarMapa() {

    let mascota_propia = L.icon({
      iconUrl: this.mascota.profile_pic,
      iconSize: [20, 20]
    });

    document.getElementById('contenedor_mapa').innerHTML = `<div id="leafletmap" style="width: 80vw; height: 80vh;"></div>`;

    this.map = L.map("leafletmap", {
      center: [this.mascota.domicilio.latitud_map, this.mascota.domicilio.longitud_map],
      zoom: 14
    });

    L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", { attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors' })
      .addTo(this.map);

    let m: L.Marker = L.marker([this.mascota.domicilio.latitud_map, this.mascota.domicilio.longitud_map], { icon: mascota_propia });

    m.addTo(this.map);
    m.getElement().style.borderRadius = '50%';
    m.getElement().style.border = '2px solid red';

    this.mascotas.forEach(mascota => {

      let mascota_ajena = L.icon({
        iconUrl: (mascota.profile_pic) ? mascota.profile_pic : environment.profile_pic_default,
        iconSize: [20, 20]
      });


      if (calcular_edad(new Date(mascota.fecha_nac)) > 0) {
        mascota.edad = calcular_edad(new Date(mascota.fecha_nac));
      } else {
        mascota.edad = 1;
      }

      let marcador = L.marker([mascota.domicilio.latitud_map, mascota.domicilio.longitud_map], { icon: mascota_ajena })
        .addTo(this.map)
        .on('click', (evento: any) => {

          this.traerMascotaClickeada(evento.latlng);

        });

      // set style de los puntos mascotas en el mapa.
      marcador.getElement().style.borderRadius = '50%';
      marcador.getElement().style.border = 'solid black 2px';

    });
  }

  traerMascotaClickeada(coordenada: L.LatLng) {

    for (let i = 0; i < this.mascotas.length; i++) {
      if (this.mascotas[i].domicilio.latitud_map == coordenada.lat && this.mascotas[i].domicilio.longitud_map == coordenada.lng) {
        this.mascota_seleccionada = this.mascotas[i];

        // pone la foto por defecto en el modal en caso que no tenga imagen de perfil.
        this.mascota_seleccionada.profile_pic = (this.mascota_seleccionada.profile_pic) ? this.mascota_seleccionada.profile_pic : environment.profile_pic_default;

        $('#modal_mascota').modal('show');
        break;
      }
    }
  }

  irMascota() {

    $('#modal_mascota').modal('dispose'); // Cerrar modal.
    $('.modal-backdrop').remove(); //Desaparecer el background.

    this.pet_srv.setMascotaAjena(this.mascota_seleccionada);
    this.router.navigateByUrl('/pet');

  }

}
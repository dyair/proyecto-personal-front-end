import { Component, OnInit, OnDestroy } from '@angular/core';
import { Mascota, Sexo, Raza, Provincia, Temperamento, Localidad, Mensaje, Usuario } from 'src/app/modelos/modelos';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Mascota_http_Service } from 'src/app/servicios/mascota_http.service';
import { HttpParams } from '@angular/common/http';
import { PetService } from '../../../servicios/pet.service';
import { Router } from '@angular/router';
import { forkJoin, Subscription } from 'rxjs';
import { Tamanio, Comunicacion } from '../../../modelos/modelos';
import { WebsocketService } from '../../../servicios/sockets/websocket.service';
import { UserService } from '../../../servicios/usuario.service';
import { flatMap, map } from 'rxjs/operators';

declare const $;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit, OnDestroy {

  public mascota: Mascota;
  private filtros_buscar: FormGroup;
  private sexos: Sexo[];
  private temperamentos: Temperamento[];
  private razas: Raza[];
  private provincias: Provincia[];
  private localidades: Localidad[];
  private tamanios: Tamanio[];
  private subscripcionMensajesSocket: Subscription;
  private subscripcionMascota: Subscription;
  private total_sms_no_vistos: number;
  private mensajes_no_vistos: Mensaje[];

  constructor(private fb: FormBuilder, private http_srv: Mascota_http_Service,
    private router: Router, private pet_srv: PetService, private ws: WebsocketService,
    private usuario_srv: UserService) { }

  ngOnInit() {

    this.mensajes_no_vistos = [];

    this.subscripcionMascota = this.pet_srv.mascota_propia.
      pipe(flatMap((m: Mascota) => {

        let mascota_storage: Mascota = this.pet_srv.getMascotaStorage();

        if (!m && !mascota_storage) this.subscripcionMascota.unsubscribe();

        this.mascota = (m) ? m : mascota_storage;

        return this.http_srv.get(`/mascota/${this.mascota.id_mascota}/notificacion/sms_no_visto`);

      }))
      .subscribe((comunicaciones_sms_no_vistos: Comunicacion[]) => {

        this.total_sms_no_vistos = 0;
        this.mensajes_no_vistos = [];

        for (let i = 0; i < comunicaciones_sms_no_vistos.length; i++) {
          for (let x = 0; x < comunicaciones_sms_no_vistos[i].mensajes.length; x++) {
            this.mensajes_no_vistos.push(comunicaciones_sms_no_vistos[i].mensajes[x]);
            this.total_sms_no_vistos++;
          }
        }

        if (comunicaciones_sms_no_vistos.length === 0) {
          // $('#dropdown_notificaciones').hide();
          $("#boton_sms_notif").attr("disabled", true);
          $('#img_notif').css('opacity', '0.7');
        }

      }, (e: Error) => {
        console.error(e.message, '. Error observable con mascota en navbar.');
      });

    // Efecto hover sobre las imágenes.
    $(document).ready(function () {
      $('.img_estilo_map').hover(function () {
        $('#img_map').css('-webkit-filter', 'contrast(250%)');
      }, function () {
        // on mouseout, reset the background colour
        $('#img_map').css('-webkit-filter', '');
      });

      $('.img_estilo_sms').hover(function () {
        $('#img_sms').css('-webkit-filter', 'contrast(250%)');
      }, function () {
        // on mouseout, reset the background colour
        $('#img_sms').css('-webkit-filter', '');
      });

    });

    this.filtros_buscar = this.fb.group({
      nombre: [''],
      raza: [''],
      sexo: [''],
      radio: [''],
      tamanio: [''],
      edad_desde: [0],
      edad_hasta: [15],
      temperamento: [''],
      provincia: [''],
      localidad: ['']
    });

    // Dejar escuchando notificaciones de mensajes vía sockets
    this.subscripcionMensajesSocket = this.pet_srv.mensaje_mascota
      .subscribe((sms: Mensaje) => {

        if (!sms) return;

        console.log(sms);

        if (this.router.url !== '/social/messages') {

          this.mensajes_no_vistos.push(sms);
          this.total_sms_no_vistos++;

          $("#boton_sms_notif").attr("disabled", false);    // permite no dejarlo disabled al botón como al ppio cuando sms_no_vistos = 0;

        }

      }, (e: Error) => {
        console.error(e.message);
      });

  }

  ngOnDestroy() {
    this.subscripcionMensajesSocket.unsubscribe();
    this.subscripcionMascota.unsubscribe();
  }

  abrirModalBusqueda() {

    let id_pais: number = this.pet_srv.getMascotaStorage().pais.id_pais;

    forkJoin(
      this.http_srv.get('/configuracion/sexo'),
      this.http_srv.get('/configuracion/tamanio'),
      this.http_srv.get('/configuracion/raza'),
      this.http_srv.get(`/configuracion/provincia/${id_pais}`),
      this.http_srv.get('/configuracion/temperamento')
    )
      .subscribe(([sexos, tamanios, razas, provincias, temperamentos]) => {

        this.sexos = [];
        this.razas = [];
        this.provincias = [];
        this.temperamentos = [];
        this.tamanios = [];

        this.sexos = sexos;
        this.razas = razas;
        this.provincias = provincias;
        this.temperamentos = temperamentos;
        this.tamanios = tamanios;

        $('#exampleModalScrollable').modal('show');

      }, (e: Error) => {
        console.error(e);
      });

  }

  getLocalidadByProvincia(id_prov: number) {

    this.http_srv.get(`/configuracion/localidad/${id_prov}`)
      .subscribe((localidades: Localidad[]) => {

        this.localidades = [];
        this.localidades = localidades;

      }, e => {
        console.log(e.error);
      });
  }

  establecer_filtros() {
    // this.buscarMascota();
  }

  reiniciarFiltro() {
    this.filtros_buscar.reset();
  }

  buscarMascota() {

    let usuario: Usuario = this.usuario_srv.getUsuarioStorage();

    // Setteo para que busque las mascotas en el mapa según los filtros establecidos.
    let parametros: HttpParams = new HttpParams()
      .set('raza', this.filtros_buscar.get('raza').value)
      .set('nombre', this.filtros_buscar.get('nombre').value)
      .set('sexo', JSON.stringify(this.mascota.sexo.id_sexo))
      .set('tamanio', this.filtros_buscar.get('tamanio').value)
      .set('temperamento', this.filtros_buscar.get('temperamento').value)
      .set('edad_desde', this.filtros_buscar.get('edad_desde').value)
      .set('edad_hasta', this.filtros_buscar.get('edad_hasta').value)
      .set('provincia', this.filtros_buscar.get('provincia').value)
      .set('localidad', this.filtros_buscar.get('localidad').value)
      .set('radio', this.filtros_buscar.get('radio').value);

    this.http_srv.getSearch(`/mascota/${this.mascota.id_mascota}/search`, parametros)
      .pipe(map((mascotas: Mascota[]) => mascotas.filter(m => m.usuario.id !== usuario.id)))
      .subscribe((pets: Mascota[]) => {

        console.log(pets);

        // uso filter para no traer mascotas del mismo dueño. Podría hacerlo en el back, pero debería 
        // cambiar la llamada para mandar el id del usuario. Eso ya lo tengo acá así que me lo ahorro

        this.pet_srv.setBusquedaMascotas(pets);
        this.router.navigateByUrl('/search');

      }, (e: Error) => {
        console.log(e.message);
      });
  }

  validarRangoEdad() {

    let desde: number = this.filtros_buscar.get('edad_desde').value;
    let hasta: number = this.filtros_buscar.get('edad_hasta').value;

    if (desde >= hasta) this.filtros_buscar.get('edad_desde').setValue(hasta);

  }

  cerrarSesion() {

    this.ws.cerrarTrato(() => {
      this.pet_srv.cerrarSesionMascota();
      this.pet_srv.removeUsuarioStorage();  // limpiar storage usuario.
      this.router.navigateByUrl('/login');
    });

  }

  irMapa() {

    this.pet_srv.setBusquedaMascotas(undefined);
    this.router.navigateByUrl('/map/view');

  }

  irMensaje() {

    this.router.navigateByUrl('/social/contacts');

  }

  ponerACeroContadorSmsNoVistos() {
    this.total_sms_no_vistos = 0;
  }

  IrComunicacionDelMensaje(sms: Mensaje) {

    let id_mensajes_no_vistos: number[] = []; // array con id de mensajes no vistos.
    let id_com: number = sms.cabecera_sms_id;
    let com_elegida: Comunicacion;

    this.mensajes_no_vistos.forEach(msj => {

      if (sms.cabecera_sms_id === msj.cabecera_sms_id) id_mensajes_no_vistos.push(msj.id_mensaje);

    });

    this.http_srv.get(`/mascota/${this.mascota.id_mascota}/comunicacion/${id_com}`)
      .pipe(flatMap((com: Comunicacion) => {

        com_elegida = com;

        let mensajes_id = { mensajes_id: id_mensajes_no_vistos };

        return this.http_srv.put(`/mascota/${this.mascota.id_mascota}/set_sms_visto`, mensajes_id);

      }))
      .subscribe(() => {

        this.pet_srv.setComMascota(com_elegida);
        this.router.navigateByUrl('/social/messages');

      }, (e: Error) => {
        console.log(e.message);
      });

  }

  irMascota() {
    this.pet_srv.cerrarSesionMascota();
    this.router.navigateByUrl('/browser');
  }

  editarDatos() {
    this.router.navigateByUrl('/edit');
  }

}
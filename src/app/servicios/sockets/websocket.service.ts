import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Socket } from 'ngx-socket-io';

@Injectable()

export class WebsocketService {

  private leer_nuevo_mensaje: string = 'leer_nuevo_mensaje';
  private nueva_mascota_socket: string = 'nueva_mascota_socket';
  private enviar_nuevo_mensaje: string = 'enviar_nuevo_mensaje';
  private notificacion_de_un_match: string = 'notificacion_de_un_match';

  constructor(private socket: Socket) {

    this.chequearStatus();

  }

  chequearStatus() {

    this.socket.on('connect', () => {

    });

    this.socket.on('disconnect', () => {
      
    });

  }

  cerrarTrato(callback: Function) {

    this.socket.disconnect();
    callback();

  }

  enviarDatos(evento: string, data?: any, callback?: Function) {

    this.socket.emit(evento, data, callback);

  }

  escucharEventos(evento: string): Observable<Object> {

    return this.socket.fromEvent(evento);

  }

  //métodos get()

  getLeer_nuevo_mensaje(): string {
    return this.leer_nuevo_mensaje;
  }

  getNueva_mascota_socket(): string {
    return this.nueva_mascota_socket;
  }

  getEnviar_nuevo_mensaje(): string {
    return this.enviar_nuevo_mensaje;
  }

  getNotificacion_de_un_match(): string {
    return this.notificacion_de_un_match;
  }

}

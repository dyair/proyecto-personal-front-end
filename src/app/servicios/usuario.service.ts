import { Usuario } from '../modelos/modelos';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  constructor() {

  }

  estaLogueado(): boolean {

    try {
      
      let token: string = this.getUsuarioStorage().token;

      return true;

    } catch (error) {
      return false
    }

  }

  limpiarStorage() {
    sessionStorage.clear();
  }

  getUsuarioStorage(): Usuario {
    return JSON.parse(sessionStorage.getItem('usuario'));
  }

  // carga usuario al sessionStorage.
  setUsuarioStorage(us: Usuario) {
    sessionStorage.setItem('usuario', JSON.stringify(us));
  }

}
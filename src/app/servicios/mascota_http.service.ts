import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { map } from 'rxjs/operators'
import { UserService } from './usuario.service';

@Injectable({
  providedIn: 'root'
})

export class Mascota_http_Service {

  constructor(private _http: HttpClient, private usr_srv: UserService) { }

  // cada http call retorna un observable de tipo (Observable <any>).

  get(path: string) {

    const headers_json = new HttpHeaders({
      'Authorization': this.usr_srv.getUsuarioStorage().token,
      'Content-Type': 'application/json'
    });

    return this._http.get(environment.linkUrl + path, { headers: headers_json })
      .pipe(map(data => {
        return data['contenido'];
      }));
  }

  getSearch(path: string, parametros: HttpParams) {

    const headers_json = new HttpHeaders({
      'Authorization': this.usr_srv.getUsuarioStorage().token,
      'Content-Type': 'application/json'
    });

    return this._http.get(environment.linkUrl + path, { headers: headers_json, params: parametros })
      .pipe(map(data => {
        return data['contenido'];
      }));
  }

  put(path: string, elemento: Object) {

    const headers_json = new HttpHeaders({
      'Authorization': this.usr_srv.getUsuarioStorage().token,
      'Content-Type': 'application/json'
    });

    return this._http.put(environment.linkUrl + path, JSON.stringify(elemento), { headers: headers_json })
      .pipe(map(data => {
        return data['contenido'];
      }));

  }

  putFormData(path: string, elemento: Object) {

    const headers_multipart = new HttpHeaders({
      // 'Content-Type': 'multipart/form-data',
      'Authorization': this.usr_srv.getUsuarioStorage().token
    });

    return this._http.put(environment.linkUrl + path, JSON.stringify(elemento), { headers: headers_multipart })
      .pipe(map(data => {
        return data['contenido'];
      }));

  }

  delete(path: string, id_elemento: number) {

    //hacer un delete para json y otro para formdata

    const headers_json = new HttpHeaders({
      'Authorization': this.usr_srv.getUsuarioStorage().token,
      'Content-Type': 'application/json'
    });

    return this._http.delete(environment.linkUrl + path + '/' + id_elemento, { headers: headers_json })
      .pipe(map(data => {
        return data['contenido'];
      }));

  }

  postJSON_no_auth(path: string, elemento: Object) {

    const headers_json = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    return this._http.post(environment.linkUrl + path, elemento, { headers: headers_json })
      .pipe(map(data => {
        return data['contenido'];
      }));

  }

  postJSON(path: string, elemento: Object) {

    //debe llegar el elemento en modos string, ya parseado con JSON.stringify() o no? revisar

    const headers_json = new HttpHeaders({
      'Authorization': this.usr_srv.getUsuarioStorage().token,
      'Content-Type': 'application/json'
    });

    return this._http.post(environment.linkUrl + path, elemento, { headers: headers_json })
      .pipe(map(data => {
        return data['contenido'];
      }));

  }

  postFormData(path: string, elemento: FormData) {

    const headers_multipart = new HttpHeaders({
      // 'Content-Type': 'multipart/form-data',
      'Authorization': this.usr_srv.getUsuarioStorage().token
    });

    return this._http.post(environment.linkUrl + path, elemento, { headers: headers_multipart })
      .pipe(map(data => {
        return data['contenido'];
      }));

  }

}

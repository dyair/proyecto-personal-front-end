import { Injectable } from '@angular/core';
import { Router, CanLoad } from '@angular/router';
import { UserService } from '../usuario.service';

@Injectable()

export class LoginGuardService implements CanLoad {

  constructor(private _userSrv: UserService, private router: Router) { }

  canLoad() {
    
    if (!this._userSrv.estaLogueado()) {

      this._userSrv.limpiarStorage();
      this.router.navigateByUrl('/login');
      return false;

    } else {

      return true;
    }
  }

}

import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { PetService } from '../pet.service';

@Injectable()

export class PetGuardService implements CanActivate {

  constructor(private router: Router, private pet_srv: PetService) { }

  canActivate() {

    if (this.pet_srv.getMascotaStorage()) {
      return true;
    } else {
      this.router.navigateByUrl('/login');
      return false;
    };

  }

}

@Injectable()

export class PetAjenaGuardService implements CanActivate {

  constructor(private router: Router, private pet_srv: PetService) { }

  canActivate() {

    if (this.pet_srv.getMascotaAjenaStorage()) {
      return true;
    } else {
      this.router.navigateByUrl('/home');
      return false;
    };

  }

}

@Injectable()

export class ComunicacionGuardService implements CanActivate {

  constructor(private router: Router, private pet_srv: PetService) { }

  canActivate() {

    if (this.pet_srv.getComElegidaStorage()) {
      return true;
    } else {
      this.router.navigateByUrl('/home');
      return false;
    }

  }

}
import { BehaviorSubject } from 'rxjs';
import { Mascota, Mensaje, Comunicacion } from '../modelos/modelos';

export class PetService {

  private mascota_busqueda_BS: BehaviorSubject<Mascota[]> = new BehaviorSubject([]);
  public busqueda_mascotas = this.mascota_busqueda_BS.asObservable();

  private mascota_sms_BS: BehaviorSubject<Mensaje> = new BehaviorSubject(null);
  public mensaje_mascota = this.mascota_sms_BS.asObservable();

  private mascota_com_BS: BehaviorSubject<Comunicacion> = new BehaviorSubject(null);
  public com_elegida = this.mascota_com_BS.asObservable();

  private mascota_ajena_elegida: BehaviorSubject<Mascota> = new BehaviorSubject(null);
  public mascota_ajena = this.mascota_ajena_elegida.asObservable();

  private mascota_propia_elegida: BehaviorSubject<Mascota> = new BehaviorSubject(null);
  public mascota_propia = this.mascota_propia_elegida.asObservable();

  constructor() { }

  setBusquedaMascotas(mascotas: Mascota[]) {

    this.mascota_busqueda_BS.next(mascotas);

  }

  setSmsMascota(sms: Mensaje) {

    this.mascota_sms_BS.next(sms);

  }

  setComMascota(com: Comunicacion) {

    this.setComElegidaStorage(com);
    this.mascota_com_BS.next(com);

  }

  setMascotaAjena(m: Mascota) {

    this.setMascotaAjenaStorage(m);
    this.mascota_ajena_elegida.next(m);

  }

  setMascotaPropia(m: Mascota) {

    this.setMascotaStorage(m);
    this.mascota_propia_elegida.next(m);

  }

  setMascotaStorage(m: Mascota) {
    sessionStorage.setItem('mascota', JSON.stringify(m));
  }

  getMascotaStorage(): Mascota | undefined {

    // try-catch usado para que no error este método en el observable del navbar.

    try {
      return JSON.parse(sessionStorage.getItem('mascota'));
    } catch (error) {
      return undefined;
    }

  }

  setMascotaAjenaStorage(m: Mascota) {
    sessionStorage.setItem('mascota_ajena', JSON.stringify(m));
  }

  getMascotaAjenaStorage(): Mascota {
    return JSON.parse(sessionStorage.getItem('mascota_ajena'));
  }

  setComElegidaStorage(c: Comunicacion) {
    sessionStorage.setItem('comunicacion_elegida', JSON.stringify(c));
  }

  getComElegidaStorage(): Comunicacion {
    return JSON.parse(sessionStorage.getItem('comunicacion_elegida'));
  }

  removeComElegidaStorage() {
    sessionStorage.removeItem('comunicacion_elegida');
  }

  removeMascotaAjenaElegidaStorage() {
    sessionStorage.removeItem('mascota_ajena');
  }

  removeMascotaStorage() {
    sessionStorage.removeItem('mascota');
  }

  removeUsuarioStorage() {
    sessionStorage.removeItem('usuario');
  }

  cerrarSesionMascota() {

    this.setBusquedaMascotas(undefined);
    this.setComElegidaStorage(undefined);
    this.setComMascota(undefined);
    this.setMascotaAjena(undefined);
    this.setMascotaPropia(undefined);   // esto está disparando el error del token cuando cambio de mascota.

    this.removeComElegidaStorage();
    this.removeMascotaAjenaElegidaStorage();
    this.removeMascotaStorage();
  }

}
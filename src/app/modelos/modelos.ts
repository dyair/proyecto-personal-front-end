export class Mascota {
    public nombre_mascota: string;
    public id_mascota?: number;
    public profile_pic?: string;
    public domicilio_id?: number;
    public biografia?: string;
    public fecha_nac?: Date;
    public tamanio_id?: number;
    public sexo_id?: number;
    public tipo_id?: number;
    public raza_id?: number;
    public edad?: number;
    public usuario_id?: number;

    public domicilio?: Domicilio;
    public sexo?: Sexo;
    public tamanio?: Tamanio;
    public temperamento?: Temperamento;
    public tipo?: Tipo;
    public usuario?: Usuario;
    public raza?: Raza;
    public pais?: Pais;
    public localidad?: Localidad;
    public provincia?: Provincia
}

export class Usuario {
    public nombre: string;
    public id?: number;
    public password: string;
    public email: string;
    public token?: string;
    public google: boolean;
}

export interface Relacion {
    id_relacion?: number;
    fecha?: Date;
    match?: boolean;
    relacion_detalle_destino?: Relacion_detalle;
    relacion_detalle_origen?: Relacion_detalle;
    relacion_detalle_origen_id?: number;
    relacion_detalle_destino_id?: number;
}

export interface Relacion_detalle {
    id_relacion_detalle?: number;
    mascota_id?: number;
    mascota?: Mascota;
    like?: boolean;
    visto?: boolean;
}

export class Post {
    public id_post?: number;
    public descripcion: string;
    public fecha_publicacion?: Date;
    public archivos?: File[];
    public archivos_array?: string[];
    public mascota_id: number;
}

export interface Domicilio {
    localidad_id?: number;
    id_domicilio?: number;
    calle: string;
    numero: number;
    piso?: number;
    dpto?: string;
    latitud_map: number;
    longitud_map: number;
    mascotas?: Mascota[];
}

export interface latlgn {
    lat: number;
    lng: number;
}

export interface Tamanio {
    id_tamanio: number;
    nombre_tamanio: string;
}

export interface Sexo {
    id_sexo: number;
    nombre_sexo: string;
}

export interface Tipo {
    id_tipo_mascota: number;
    nombre_tipo_mascota: string;
}

export interface Pais {
    id_pais: number;
    nombre_pais: string;
}

export interface Raza {
    id_raza: number;
    nombre_raza: string;
}

export interface Temperamento {
    id_temperamento: number;
    nombre_temperamento: string;
}

export interface Comunicacion {
    id_comunicacion?: number;
    mascota_origen_id?: number;
    mascota_destino_id?: number;
    mascota_destino?: Mascota;
    mascota_origen?: Mascota;
    mensajes?: Mensaje[];
    ultimo_mensaje?: Mensaje
}

export interface Mensaje {
    id_mensaje?: number,
    fecha?: Date,
    contenido?: string,
    visto_origen?: boolean,
    visto_destino?: boolean
    cabecera_sms_id?: number,
    mascota_destino_id: number,
    mascota_origen_id: number,
    mascota_destino?: Mascota,
    mascota_origen?: Mascota
}

export interface Provincia {
    id_provincia: number,
    nombre_provincia: string,
    pais_id: number
}

export interface Localidad {
    id_localidad: number,
    nombre_localidad: string,
    provincia_id: number
}

export interface Error_mensaje {
    code: number;
    mensaje: string;
}


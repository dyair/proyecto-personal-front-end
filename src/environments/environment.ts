// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  linkUrl: 'https://mascota-app.herokuapp.com',  // Hosting: Heroku
  
  // google
  CLIENT_ID: '360447745298-q40v2l62qhv4dek16a3bti074fii6lnb.apps.googleusercontent.com',
  CLIENT_ID_SECRET: '5y4kacVdrqq8yprcO9zebOCt',
  profile_pic_default: 'https://image.flaticon.com/icons/png/512/194/194177.png'

};

export function calcular_edad(dob: Date): number {
  
  let diff_ms = Date.now() - dob.getTime();
  let age_dt = new Date(diff_ms);

  return Math.abs(age_dt.getUTCFullYear() - 1970);
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

import { Error_mensaje } from "src/app/modelos/modelos";

export const environment = {
  production: true,
  linkUrl: 'https://mascota-app.herokuapp.com', // ---> Hosting: Heroku

  // google
  CLIENT_ID: '360447745298-q40v2l62qhv4dek16a3bti074fii6lnb.apps.googleusercontent.com',
  CLIENT_ID_SECRET: '5y4kacVdrqq8yprcO9zebOCt',
  profile_pic_default: 'https://image.flaticon.com/icons/png/512/194/194177.png'
}

export function calcular_edad(dob: Date): number {

  let diff_ms = Date.now() - dob.getTime();
  let age_dt = new Date(diff_ms);

  return Math.abs(age_dt.getUTCFullYear() - 1970);
}

export function mensaje_error(sms_error: string): string {

  return ` 
      <div class="alert alert-danger alert-dismissible fade show container" role="alert">
        ${sms_error}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
  `;

}

export function parametrizacion_sms_error(e: any): Error_mensaje {

  console.log(e);

  let error: Error_mensaje = { code: undefined, mensaje: '' };

  switch (e.status) {
    case 401:

      error.code = e.status;
      error.mensaje = 'Credenciales erróneas.';
      break;

    case 404:

      error.code = e.status;
      error.mensaje = 'Error de contenido.';
      break;

    case 503:

      error.code = e.status;
      error.mensaje = 'Error de servicio.';
      break;

    default:

      error.code = e.status;
      error.mensaje = 'Error, intente luego.';
      break;
  }

  return error;

}
